<?php
/**
 * The main template file
 * 
 * This will be different than the page.php file (substantially)
 * See Landing Upgrade in the shared Google Drive folder
 * This page will also have featured posts on it
 */

get_header(); get_sidebar(); ?>
        <div class="col-sm-9">
        <div> <!-- featured post area -->
            <?php if ( is_home() || is_front_page() ) : 
                //this just checks if the page is the homepage..may have to edit this since it is now front-page not index
                
                //generate an SQL query
                $args = array( 
                    'posts_per_page'   => 2, //because we are pulling from 2 sites here we can get away with only 2
                    'order'            => 'DESC',
                    'orderby'          => 'date',
                    'post_status'      => 'published',
                    'category_name'    => 'featured',
                    'suppress_filters' => true, //this is needed likely because a hook is modifying the query
                );
                
                $a_posts = array(); //array for holding objects
                
                foreach( get_sites() as $site ) {
                    if( get_current_blog_id() != $site->blog_id ) { //switch between the wordpress sites in our network
                        switch_to_blog( $site->blog_id );
                    }
                    $query = new WP_Query( $args ); //query all posts based on args array
                    
                    foreach( $query->posts as $post ) {
                        $c_posts = new db_feat_posts(); //create new db_feat_posts object with each iteration
                        setup_postdata( $post ); //set up post data to get author name
                        //yeah this could probably have been done better with the use of a constructor but *shurg*
                        $c_posts->db_set_title( $post->post_title );
                        $c_posts->db_set_date( $post->post_date );
                        $c_posts->db_set_author( get_the_author() );
                        $c_posts->db_set_ID( $post->ID );
                        $c_posts->db_set_thumbnail( get_the_post_thumbnail( $post->ID, 'thumbnail' ) );
                        $c_posts->db_set_permalink( get_permalink() );
                        $c_posts->db_set_site( $site->blog_id );
                        //these are for the custom fields
                        $c_posts->db_cust_set_field_URL( get_field( 'url', $post->ID, false ) );
                        $c_posts->db_cust_set_field_ref( get_field( 'reference', $post->ID, false ) );

                        $a_posts[] = $c_posts; //new index of the post array will hold the post object
                    }
                    //clean up everything
                    restore_current_blog();
                    wp_reset_postdata();
                    wp_reset_query();
                }
                //use usort to sort the posts in DESC order (newest first)
                usort( $a_posts, function($a, $b) {
                    return strcasecmp(
                            $b->date,
                            $a->date
                    );
                });

                //remove all but the first 3 featured posts in the array
                $a_posts = array_slice( $a_posts, 0, 3 );

                ?>
                <div id="sites" class="row">
                    <a class="col-md-3 offset-md-2" href="<?php echo get_theme_mod( 'dreambig_prof_link_setting' ); ?>" title="<?php echo get_theme_mod( 'dreambig_prof_link_title_setting' ); ?>">
                        <section class="site-containers">
                            
                            <?php
                                //professional:
                                $prof_img   = esc_url( get_theme_mod( 'dreambig_prof_logo_setting' ) );
                                $prof_id    = attachment_url_to_postid( $prof_img );
                                $prof_alt   = get_post_meta( $prof_id, '_wp_attachment_image_alt', true );

                            ?>
                            <img src="<?php echo esc_url( get_theme_mod( 'dreambig_prof_logo_setting' ) ); ?>" width="200" alt="<?php echo $prof_alt; ?>" />
                            <h2 class="site-titles">Professional Work</h2>
                            <p><?php echo get_theme_mod( 'dreambig_prof_desc_setting' ); ?></p>
                        </section>
                    </a>
                    <a class="col-md-3 offset-md-1" href="<?php echo get_theme_mod( 'dreambig_dream_link_setting' ); ?>" title="<?php echo get_theme_mod( 'dreambig_dream_link_title_setting' ) ?>">
                        <section class="site-containers">
                            <?php
                                //DREAM Development:
                                $dream_img   = esc_url( get_theme_mod( 'dreambig_dream_logo_setting' ) );
                                //on large databases this attachment_url_to_postid() method can be really taxing on resources...
                                $dream_id    = attachment_url_to_postid( $dream_img );
                                $dream_alt   = get_post_meta( $dream_id, '_wp_attachment_image_alt', true );

                            ?>
                            <img src="<?php echo $dream_img ?>" width="200" alt="<?php echo $dream_alt; ?>" />
                            <h2 class="site-titles">Dream Development</h2>
                            <p><?php echo get_theme_mod( 'dreambig_dream_desc_setting' ); ?></p>
                        </section>
                    </a>
                </div>
                <div id="featured" class="row">
                    <h2> Featured Blog Posts </h2>
                    <?php if ( $a_posts ): ?>
                        <div class="feat-posts">
                        <?php foreach ( $a_posts as $post ) : ?>
                            <div id="site-<?php echo $post->site . '-post-' . $post->ID; ?>" class="feat-post">
                            <?php 
                                //if a post does not have a thumbnail, just insert a blank one 
                                //note: using get_template_directory_uri() returns the url of the image
                                ?>
                                <?php if ( empty( $post->thumbnail ) ) : ?>
                                    <img width="150px" src="<?php echo get_template_directory_uri() . '/assets/images/blank.png'; ?>" alt="transparent image" style="visibility: hidden;" />
                                <?php else:
                                    echo $post->thumbnail; ?>
                                <?php endif; ?>
                                <?php if ( $post->cust_field_ref == '' ) : ?>
                                    <p>Author: <?php echo $post->author_name; ?></p>
                                <?php else : ?>
                                    <p>From: <a target="_blank" href="<?php echo $post->cust_field_URL; ?>" title="<?php echo $post->cust_field_ref; ?>"><?php echo $post->cust_field_ref; ?></p>
                                <?php endif; ?>
                                <?php
                                    //Note: the db_feat_posts class is trimming the title to 20 characters
                                    $title = $post->title;
                                ?>

                                
                                <?php //everything below this needs a bit of clean up and a better way of displaying ?>
                                <p>
                                    <a href="<?php echo $post->permalink ?>" target="_blank" title="<?php echo $title; ?>"><?php echo $title; ?></a>
                                </p>
                            </div>
                        <?php endforeach;?>
                        </div> <!--feat-posts-->
                    <?php else: ?>
                        <p style="text-align:center;">No featured posts were found</p>
                    <?php endif; ?>
                </div> <!-- .featured -->
        </div>
        </div> <!-- ..col-sm-8 -->
<?php get_footer(); endif;