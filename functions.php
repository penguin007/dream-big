<?php
/**
 * Dream Big functions and definitions
 * 
 * This theme was designed on and for WordPress 4.9.8
 * 
 * 
 * 
 */

//this is our bootstrap nav walker
//https://www.omnicoda.com/blog/wordpress-development/beginners-guide-wordpress-menus-bootstrap-navigation
//and that ^^ is my documentation on how to use it

//https://github.com/wp-bootstrap/wp-bootstrap-navwalker/tree/master
//A very handy class that allows for the incorporation of Bootstrap 4.0.0+ navigation styles with WordPress
require_once( get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php' );

require_once( get_template_directory() . '/inc/posts.php' );

//https://gist.github.com/ebinnion/7635465 <-- his code is included below
require_once( get_template_directory() . '/inc/bootstrap_wp_link_pages.php' );

//Checks to see if the user is logged in or not and if admin or contriubter
//if so, shows them the admin bar (at top of screen).
//This could be a ternary [did I spell that right?] operator
function check_admin() {
    if ( current_user_can( 'administrator' ) || current_user_can( 'contributer' ) ) {
        show_admin_bar( true );
    } else {
        show_admin_bar( false );
    }
}

//Dream Big setup
if ( ! function_exists( 'dreambig_setup' ) ) :
    function dreambig_setup() {
        //adds ability to change title from the admin area
        add_theme_support( 'title-tag' );

        //adds ability to add featured picture to the posts
        add_theme_support( 'post-thumbnails' );

        //these are the navigation menus, we have social, landing and a professinonal one + dream dev. one
        //these will likely change as we will be using a child theme...
        register_nav_menus( array(
                //Menu for the landing page
                'index-menu' => __( 'Landing Page Menu' ),
                //Menu for social links (facebook, twitter, instagram)
                'links-menu' => __( 'Social Links Menu' ), 
            ));

        add_theme_support( 'post-formats', 
            array(
                'link',
                'video',
                'image',
                'status'
            )
        );
}
endif;

//this just calls the function above
add_action( 'after_setup_theme', 'dreambig_setup' );

//Bootstrap is being pulled from local file in /css and /js directories
//this function just adds these files automaticalyl within the head tag
function db_enqueue() {
    //note all of these are the minified version just to save bandwith
    //bootstrap
    wp_enqueue_style( 'bootstrapstyle', get_template_directory_uri() . '/css/bootstrap.min.css' );

    wp_enqueue_style( 'bootstrapthemestyle', get_template_directory_uri() . '/css/bootstrap-theme.min.css' );

    wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array(), true );

    //this is the theme's default stylesheet
    /**
     * okay so adding a parameter of time should always force wordpress to use the latest stylesheet
     */
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', time() );

    //HTML 5 Shiv by aFarkas: https://github.com/aFarkas/html5shiv
    //This allows support for HTML 5 on older browsers, namely, Internet Explorer version 8 and below.
    wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/js/html5shiv.min.js' );
    wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 8' ); //adds the condition

    //insert jQuery (I'm not sure if this is needed)
    //this is likely not needed since bootstrap 4.0 comes with it
    //wp_enqueue_script( 'jquery-3.3.1', get_template_directory_uri() . '/js/jquery-3.3.1.min.js' );
}

add_action( 'wp_enqueue_scripts', 'db_enqueue' );

//creates the nav menu on the landing page
function index_main_nav() {
    wp_nav_menu( array(
            'menu'              => 'index-menu', //index-menu is landing page menu, see functions.php
            'depth'             => 3, 
            'menu_id'           => 'index-menu', //literally can be changed to whatever
            'menu_class'        => 'navbar-nav',
            'container'         => 'nav',
            'container_id'      => 'main-navbar',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'container_class'   => 'navbar navbar-expand-lg justify-content-center', //this too can be any bootstrap class
            'walker'            => new wp_bootstrap_navwalker(),
        ));
}

//To customize the Block Y and Dream Dev images on homepage, we could use customizer

function dreambig_customizer( $wp_customize ) {
    //DREAM
    //settings
    $wp_customize->add_setting( 'dreambig_dream_logo_setting', array(
        'default'   => 'green', //ignore the green, that needs to be an image path...Note to Dani: make or find a generic blank image
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting( 'dreambig_dream_desc_setting', array(
        'default'   => 'Something about Dream Development',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting( 'dreambig_dream_link_setting', array(
        'default'   => 'https://www.wordpress.org',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting( 'dreambig_dream_link_title_setting', array(
        'default'   => 'Link to my Dream Development Page',
        'transport' => 'refresh',
    ));
    
    //section
    $wp_customize->add_section( 'dream_development', array(
        'title'    => __('Dream Development', 'dream-big'),
        'priority' => 30,
    ));

    //controls
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'dreambig_dream_logo_setting', array(
        'label'     => __( 'Dream Development Logo', 'dream-big' ),
        'section'   => 'dream_development',
        'settings'  => 'dreambig_dream_logo_setting',
    )));

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'dreambig_dream_desc_setting', array(
        'label'     => __( 'Dream Development Description', 'dream-big' ),
        'section'   => 'dream_development',
        'settings'  => 'dreambig_dream_desc_setting',
    )));

    $wp_customize->add_control( new WP_Customize_Control ($wp_customize, 'dreambig_dream_link_setting', array(
        'label'     => __( 'Dream Development Link', 'dream-big' ),
        'section'   => 'dream_development',
        'settings'  => 'dreambig_dream_link_setting',
    )));

    $wp_customize->add_control( new WP_Customize_Control ($wp_customize, 'dreambig_dream_link_title_setting', array(
        'label'     => __( 'Dream Development Link Title', 'dream-big' ),
        'section'   => 'dream_development',
        'settings'  => 'dreambig_dream_link_title_setting',
    )));

    //Professional
    //settings
    $wp_customize->add_setting( 'dreambig_prof_logo_setting', array(
        'default'   => 'green', //ignore the green, that needs to be an image path...Note to Dani: make or find a generic blank image
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting( 'dreambig_prof_desc_setting', array(
        'default'   => 'Something about Professional',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting( 'dreambig_prof_link_setting', array( //doesn't work
        'default'   => 'http://drfarris.org',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting( 'dreambig_prof_link_title_setting', array( //doesn't work
        'default'   => 'Link to my professional site',
        'transport' => 'refresh',
    ));
    
    //section
    $wp_customize->add_section( 'professional', array(
        'title'    => __('Professional', 'dream-big'),
        'priority' => 30,
    ));

    //controls
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'dreambig_prof_logo_setting', array(
        'label'     => __( 'Professional Logo', 'dream-big' ),
        'section'   => 'professional',
        'settings'  => 'dreambig_prof_logo_setting',
    )));

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'dreambig_prof_desc_setting', array(
        'label'     => __( 'Professional Description', 'dream-big' ),
        'section'   => 'professional',
        'settings'  => 'dreambig_prof_desc_setting',
    )));

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'dreambig_prof_link_setting', array( //doesn't work
        'label'     => __( 'Professional Website Link', 'dream-big' ),
        'section'   => 'professional',
        'settings'  => 'dreambig_prof_link_setting',
    )));

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'dreambig_prof_link_title_setting', array( //doesn't work
        'label'     => __( 'Professional Website Link Title', 'dream-big' ),
        'section'   => 'professional',
        'settings'  => 'dreambig_prof_link_title_setting',
    )));

}
add_action( 'customize_register', 'dreambig_customizer' );

function dreambig_widgets_init() {
    register_sidebar( array(
        'name'          => 'Site Info',
        'id'            => 'site-info-widget',
        'before_widget' => '<section>',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));
}
add_action( 'widgets_init', 'dreambig_widgets_init' );