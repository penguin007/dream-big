<?php
    class db_feat_posts {

        public $title;
        public $ID;
        public $author_name;
        public $permalink;
        public $thumbnail;
        public $date;
        public $site;
        public $cust_field_URL;
        public $cust_field_ref;
        //==getters===

        public function db_get_title() {
            return $this->title;
        }

        public function db_get_ID() {
            return $this->ID;
        }

        public function db_get_author() {
            return $this->author_name;
        }

        public function db_get_permalink() {
            return $this->permalink;
        }

        public function db_get_thumbnail() {
            return $this->thumbnail;
        }

        public function db_get_date() {
            return $this->date;
        }

        public function db_get_site() {
            return $this->site;
        }

        public function db_cust_get_URL() {
            return $this->cust_field_URL;
        }

        public function db_cust_get_ref() {
            return $this->cust_field_ref;
        }
        //==setters==

        public function db_set_title( $in ) {
            //cut down the title length to 20 characters
            $this->title = (strlen($in) > 20) ? substr($in, 0, 20) . '...' : $in;;
        }

        public function db_set_ID( $in ) {
            $this->ID = $in;
        }

        public function db_set_author( $in ) {
            $this->author_name = $in;
        }

        public function db_set_permalink( $in ) {
            $this->permalink = $in;
        }

        public function db_set_thumbnail( $in ) {
            $this->thumbnail = $in;
        }

        public function db_set_date( $in ) {
            $this->date = $in;
        }

        public function db_set_site( $in ) {
            $this->site = $in;
        }

        public function db_cust_set_field_URL( $in ) {
            $this->cust_field_URL = $in;
        }

        public function db_cust_set_field_ref( $in ) {
            $this->cust_field_ref = $in;
        }
    }
?>