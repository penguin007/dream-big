<?php
/**
 * The template file for displaying static web pages
 */
get_header();
get_sidebar();
?>
        <main class="col-sm-9">
        <?php
        while ( have_posts() ): the_post();
            //just calls the template parts file that cooresponds with the page
            //I think that's for reusability
            //see file: template-parts/content-page.php
            get_template_part( 'template-parts/content', 'page' );
        endwhile;
        ?>
        </main>
<?php get_footer(); ?>
<?php //get header, footer, sidebar all just find the php files and add them so like require_once '';
      //note...get_sidebar(); might have to come before the content area
?>