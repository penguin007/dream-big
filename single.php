<?php
/**
 * The template for displaying all single posts
 * 
 * 
 */
get_header();
get_sidebar();
?>
        <main class="col-sm-9">
        <?php
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', get_post_type() );

            the_post_navigation( array(
                'prev_text' => '<p>&larr; Previous Post...%title</p>',
                'next_text' => '<p>Next Post...%title &rarr;</p>',
            ));

            /*
            if ( comments_open() || get_comments_number() ):
                comments_template();
            endif;
            */

        endwhile;
        ?>
        </main>
<?php
get_footer();