<?php
/**
 * Template part for displaying posts
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    //if the standard format...
    if ( get_post_format() == false ) {
        //bootstrap classes can be added here
        if ( is_singular() ) :
            the_title( '<h1>', '</h1>' );
        else :
            the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        endif;
        ?>
        <p>
            By: <?php the_author_meta( 'display_name' ); ?>
            | <?php the_date(); ?>
        </p>
        <p>
            <?php if ( get_the_modified_date() != get_the_date() ) : ?>
                Last Modified: <?php the_modified_date(); ?>
            <?php endif; ?>
        </p>
        <?php echo get_avatar( the_author_meta( 'ID', 64 ) ); ?>
        <p><?php the_content(); ?></p>
        <?php wp_link_pages();

    } elseif ( get_post_format() == 'link') {
        if ( is_singular() ) :
            the_title( '<h1>', '</h1>' );
        else :
            the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        endif;
        ?>
        <?php echo get_avatar( the_author_meta( 'ID', 64 ) ); ?>
        <p>
            <b>By:</b> <?php the_author_meta( 'display_name' ); ?>
            | <?php the_date(); ?>
        </p>
        
            <?php if ( get_the_modified_date() != get_the_date() ) : ?>
                <p><b>Last Modified:</b> <?php the_modified_date(); ?></p>
            <?php endif; ?>
        
        <p><?php the_content(); ?>
        <h4><a href="<?php the_field( 'url', $post->ID, false ); ?>">Continue Reading</a></h4>
        <p><b>Source Author:</b> <?php the_field( 'reference', $post->ID, false ); ?></p>
        <?php wp_link_pages();        
    } else {
       //bootstrap classes can be added here
        if ( is_singular() ) :
            the_title( '<h1>', '</h1>' );
        else :
            the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        endif;
        ?>
        <p>
            By: <?php the_author_meta( 'display_name' ); ?>
            | <?php the_date(); ?>
        </p>
        <p>
            <?php if ( get_the_modified_date() != get_the_date() ) : ?>
                Last Modified: <?php the_modified_date(); ?>
            <?php endif; ?>
        </p>
        <?php echo get_avatar( the_author_meta( 'ID', 64 ) ); ?>
        <p><?php the_content(); ?></p>
        <?php wp_link_pages();
    }
    ?>
</article><!-- #post-<?php the_ID(); ?> -->
<?php
if ( ! is_single() ) {
    echo "<hr />";
}
