<?php
/**
 * Template for the sidebar which will show a quote
 * 
 * 
 * 
 * First it checks to see if the plugin is installed only then will it display the quote
 * Got to work better with error handling here..
 * 
 * Right now all it does is call a simple shortcode
 * More info on shortcodes here: https://codex.wordpress.org/shortcode 
 */
?>
        <div class="col-sm-3 col-sm-height" id="sidebar"> <!-- some CSS media query here to make it hidden on mobile devices or bootstrap-->
            <?php
            //this page needs to be styled (bootstrap and also add divs)
            //this is needed because to is_plugin_active only works with it, so bring it to the template file
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            if ( is_plugin_active( 'quotes-collection/quotes-collection.php' ) ) :
                //MUST HAVE quotes collection installed and activated
            ?>
                <div id="sidebar-quote" class="sidebar-quote"> <!--have to make id change if we want more than one quote-->
                    <?php echo do_shortcode('[quotcoll orderby="random" limit=1]'); //note, make this more dynamic as well?>
                </div>
            <?php else : ?>
                <div>
                    <!-- For now, just throwing an error in it, can change later -->
                    <p>Please ensure that the plugin "Quotes Collection" is installed and active!</p>
                </div>
            <?php endif; ?>
        <?php
            //This is wrapped in a <section> element
            if ( is_active_sidebar('site-info-widget')) {
                dynamic_sidebar( 'site-info-widget' );
            }
        ?>
        </div> <!-- .sidebar -->
